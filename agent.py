from gameobjects import GameObject
from move import Move, Direction


class Agent:
    def __init__(self):
        self.next_elem = None
        self.next_to_tail = None

    class Node:
        def __init__(self, parent=None, position=None, direction_head=None):
            self.parent = parent
            self.position = position
            self.direction_head = direction_head

            self.g = 0
            self.h = 0
            self.f = 0

        def __eq__(self, other):
            return self.position == other.position

    def a_star(self, board, snake_head, direction, food, snake_tail):
        start_node = self.Node(None, snake_head, direction)
        end_nodes = []

        tail = self.Node(None, snake_tail, None)

        for food_elem in food:
            end_nodes.append(self.Node(None, food_elem, None))
        open_list = []
        closed_list = []
        open_list.append(start_node)
        while len(open_list) > 0:
            current_node = min(open_list, key=lambda k: k.f)
            open_list.remove(current_node)
            closed_list.append(current_node)

            if current_node == tail:
                path_to_tail = []
                current = current_node
                while current is not None:
                    path_to_tail.append(current.position)
                    current = current.parent
                self.next_to_tail = path_to_tail[-2]

            if current_node in end_nodes:
                path = []
                current = current_node
                while current is not None:
                    path.append(current.position)
                    current = current.parent
                self.next_elem = path[-2]
                break
            children = []
            for new_position in [(0, -1), (0, 1), (-1, 0), (1, 0)]:
                node_position = (current_node.position[0] + new_position[0], current_node.position[1] + new_position[1])

                if not ((0 <= node_position[0] < len(board[0])) and (0 <= node_position[1] < len(board))):
                    continue
                if board[node_position[0]][node_position[1]] is GameObject.WALL or \
                        board[node_position[0]][node_position[1]] is GameObject.SNAKE_BODY:
                    continue

                if new_position == (0, -1) and current_node.direction_head is not Direction.SOUTH:
                    new_node = self.Node(current_node, node_position, Direction.NORTH)
                    children.append(new_node)
                elif new_position == (0, 1) and current_node.direction_head is not Direction.NORTH:
                    new_node = self.Node(current_node, node_position, Direction.SOUTH)
                    children.append(new_node)
                elif new_position == (-1, 0) and current_node.direction_head is not Direction.EAST:
                    new_node = self.Node(current_node, node_position, Direction.WEST)
                    children.append(new_node)
                elif new_position == (1, 0) and current_node.direction_head is not Direction.WEST:
                    new_node = self.Node(current_node, node_position, Direction.EAST)
                    children.append(new_node)
                else:
                    continue

            for child in children:
                if not self.child_in_closed_list(child, closed_list):
                    continue
                if not self.child_in_open_list(child, open_list):
                    continue

                child = self.child_update(child, current_node, end_nodes)
                open_list.append(child)

    def get_move(self, board, score, turns_alive, turns_to_starve, direction, head_position, body_parts):

        food = self.get_food(board)

        if score > 1:
            self.a_star(board, head_position, direction, food, [body_parts[-1]])
            if self.next_elem:
                return self.get_move_direction(
                    (self.next_elem[0] - head_position[0], self.next_elem[1] - head_position[1]), direction)
            else:
                if self.next_to_tail:
                    return self.get_move_direction(
                        (self.next_to_tail[0] - head_position[0], self.next_to_tail[1] - head_position[1]), direction)
                else:
                    return self.get_free_move(head_position, direction, board)
        else:
            self.a_star(board, head_position, direction, food, [(-1, -1)])
            if self.next_elem:
                return self.get_move_direction(
                    (self.next_elem[0] - head_position[0], self.next_elem[1] - head_position[1]), direction)
            else:
                if self.next_to_tail:
                    return self.get_move_direction(
                        (self.next_to_tail[0] - head_position[0], self.next_to_tail[1] - head_position[1]), direction)
                else:
                    return self.get_free_move(head_position, direction, board)

    """This function behaves as the 'brain' of the snake. You only need to change the code in this function for
        the project. Every turn the agent needs to return a move. This move will be executed by the snake. If this
        functions fails to return a valid return (see return), the snake will die (as this confuses its tiny brain
        that much that it will explode). The starting direction of the snake will be North.

        :param board: A two dimensional array representing the current state of the board. The upper left most
        coordinate is equal to (0,0) and each coordinate (x,y) can be accessed by executing board[x][y]. At each
        coordinate a GameObject is present. This can be either GameObject.EMPTY (meaning there is nothing at the
        given coordinate), GameObject.FOOD (meaning there is food at the given coordinate), GameObject.WALL (meaning
        there is a wall at the given coordinate. TIP: do not run into them), GameObject.SNAKE_HEAD (meaning the head
        of the snake is located there) and GameObject.SNAKE_BODY (meaning there is a body part of the snake there.
        TIP: also, do not run into these). The snake will also die when it tries to escape the board (moving out of
        the boundaries of the array)

        :param score: The current score as an integer. Whenever the snake eats, the score will be increased by one.
        When the snake tragically dies (i.e. by running its head into a wall) the score will be reset. In other
        words, the score describes the score of the current (alive) worm.

        :param turns_alive: The number of turns (as integer) the current snake is alive.

        :param turns_to_starve: The number of turns left alive (as integer) if the snake does not eat. If this number
        reaches 1 and there is not eaten the next turn, the snake dies. If the value is equal to -1, then the option
        is not enabled and the snake can not starve.

        :param direction: The direction the snake is currently facing. This can be either Direction.NORTH,
        Direction.SOUTH, Direction.WEST, Direction.EAST. For instance, when the snake is facing east and a move
        straight is returned, the snake wil move one cell to the right.

        :param head_position: (x,y) of the head of the snake. The following should always hold: board[head_position[
        0]][head_position[1]] == GameObject.SNAKE_HEAD.

        :param body_parts: the array of the locations of the body parts of the snake. The last element of this array
        represents the tail and the first element represents the body part directly following the head of the snake.

        :return: The move of the snake. This can be either Move.LEFT (meaning going left), Move.STRAIGHT (meaning
        going straight ahead) and Move.RIGHT (meaning going right). The moves are made from the viewpoint of the
        snake. This means the snake keeps track of the direction it is facing (North, South, West and East).
        Move.LEFT and Move.RIGHT changes the direction of the snake. In example, if the snake is facing north and the
        move left is made, the snake will go one block to the left and change its direction to west.
        """

    def child_in_closed_list(self, child, closed_list):
        for closed_child in closed_list:
            if child == closed_child:
                return 0
        return 1

    def child_in_open_list(self, child, open_list):
        for open_node in open_list:
            if child == open_node:
                if child.g < open_node.g:
                    open_node.g = child.g
                    open_node.parent = child.parent
                return 0
        return 1

    def child_update(self, child, current_node, end_nodes):
        child.g = current_node.g + 1
        child.h = min(2 *
                      (abs(end_node.position[0] - current_node.position[0]) + abs(
                          end_node.position[1] - current_node.position[1]))
                      for end_node in end_nodes)
        child.f = child.g + child.h
        return child

    def get_food(self, board):
        food = []
        for x in range(len(board[0])):
            for y in range(len(board)):
                if board[x][y] is GameObject.FOOD:
                    food.append((x, y))
        return food

    def get_free_move(self, head_position, direction, board):
        self.next_elem = None
        if direction is Direction.NORTH:
            if head_position[1] > 0 and board[head_position[0]][head_position[1] - 1] is GameObject.EMPTY:
                return Move.STRAIGHT
            if head_position[0] > 0 and board[head_position[0] - 1][head_position[1]] is GameObject.EMPTY:
                return Move.LEFT
            if head_position[0] < len(board) - 1 and board[head_position[0] + 1][head_position[1]] is GameObject.EMPTY:
                return Move.RIGHT
        elif direction is Direction.SOUTH:
            if head_position[1] < len(board[0]) - 1 and board[head_position[0]][head_position[1] + 1] is GameObject.EMPTY:
                return Move.STRAIGHT
            if head_position[0] > 0 and board[head_position[0] - 1][head_position[1]] is GameObject.EMPTY:
                return Move.RIGHT
            if head_position[0] < len(board[0]) - 1 and board[head_position[0] + 1][head_position[1]] is GameObject.EMPTY:
                return Move.LEFT
        elif direction is Direction.WEST:
            if head_position[0] > 0 and board[head_position[0] - 1][head_position[1]] is GameObject.EMPTY:
                return Move.STRAIGHT
            if head_position[1] > 0 and board[head_position[0]][head_position[1] - 1] is GameObject.EMPTY:
                return Move.RIGHT
            if head_position[1] < len(board[0]) - 1 and board[head_position[0]][head_position[1] + 1] is GameObject.EMPTY:
                return Move.LEFT
        elif direction is Direction.EAST:
            if head_position[0] < len(board) - 1 and board[head_position[0] + 1][head_position[1]] is GameObject.EMPTY:
                return Move.STRAIGHT
            if head_position[1] > 0 and board[head_position[0]][head_position[1] - 1] is GameObject.EMPTY:
                return Move.LEFT
            if head_position[1] < len(board[0]) - 1 and board[head_position[0]][head_position[1] + 1] is GameObject.EMPTY:
                return Move.RIGHT
        return

    def get_move_direction(self, position, direction):
        self.next_elem = None
        if position == (0, -1):
            if direction is Direction.NORTH:
                return Move.STRAIGHT
            if direction is Direction.EAST:
                return Move.LEFT
            if direction is Direction.WEST:
                return Move.RIGHT
        elif position == (0, 1):
            if direction is Direction.SOUTH:
                return Move.STRAIGHT
            elif direction is Direction.WEST:
                return Move.LEFT
            elif direction is Direction.EAST:
                return Move.RIGHT
        elif position == (-1, 0):
            if direction is Direction.WEST:
                return Move.STRAIGHT
            elif direction is Direction.NORTH:
                return Move.LEFT
            elif direction is Direction.SOUTH:
                return Move.RIGHT
        elif position == (1, 0):
            if direction is Direction.EAST:
                return Move.STRAIGHT
            elif direction is Direction.SOUTH:
                return Move.LEFT
            elif direction is Direction.NORTH:
                return Move.RIGHT

    def should_redraw_board(self):
        """
        This function indicates whether the board should be redrawn. Not drawing to the board increases the number of
        games that can be played in a given time. This is especially useful if you want to train you agent. The
        function is called before the get_move function.

        :return: True if the board should be redrawn, False if the board should not be redrawn.
        """
        return True

    def should_grow_on_food_collision(self):
        """
        This function indicates whether the snake should grow when colliding with a food object. This function is
        called whenever the snake collides with a food block.

        :return: True if the snake should grow, False if the snake should not grow
        """
        return True

    def on_die(self, head_position, board, score, body_parts):
        """This function will be called whenever the snake dies. After its dead the snake will be reincarnated into a
        new snake and its life will start over. This means that the next time the get_move function is called,
        it will be called for a fresh snake. Use this function to clean up variables specific to the life of a single
        snake or to host a funeral.

        :param head_position: (x, y) position of the head at the moment of dying.

        :param board: two dimensional array representing the board of the game at the moment of dying. The board
        given does not include information about the snake, only the food position(s) and wall(s) are listed.

        :param score: score at the moment of dying.

        :param body_parts: the array of the locations of the body parts of the snake. The last element of this array
        represents the tail and the first element represents the body part directly following the head of the snake.
        When the snake runs in its own body the following holds: head_position in body_parts.
        """
